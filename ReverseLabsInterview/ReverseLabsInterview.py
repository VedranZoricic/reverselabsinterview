import pandas as pd
from matplotlib import pyplot as plt
import seaborn as sn


def create_dict(file, delimiter=','):
    """
    Create a dictionary from file. Each line must contain a delimiter that splits it into ID-Value
    :param delimiter: delimiter that split ID from value
    :param file: file
    :return: dictionary from file
    """
    new_dict = dict()
    for line in file.readlines():
        if line != "\n":
            line_split = line.rstrip().split(delimiter)
            new_dict[line_split[0]] = line_split[1].strip()
    return new_dict


def match_samples(old, new):
    """
    Count number of matching values from 2  dictionaries
    and count number of matches for each label
    :param old: old file dictionary
    :param new: new file dictionary
    :return:Number of matching values and dictionary of
    numbers of matches for each label
    """
    matched = 0
    matched_labels_dict = dict()
    for sample in old:
        if sample in new.keys():
            if new[sample] == old[sample]:
                matched += 1
                if new[sample] in matched_labels_dict.keys():
                    matched_labels_dict[new[sample]] += 1
                else:
                    matched_labels_dict[new[sample]] = 1
    return matched, matched_labels_dict


def graph_distribution(dict1, dict2, save=None, show=None):
    """
    :param dict1: old as file dictionary
    :param dict2: new as file dictionary
    :param save: flag to save graph
    :param show: flag to show graph
    :return: None
    """
    plt.hist([list(dict1.values()), list(dict2.values())], label=['old', 'new'])
    plt.legend(loc='upper right')
    if save:
        plt.savefig('graph_distribution.png')
    if show:
        plt.show()


def graph_confusion(merged_df, save=None, show=None):
    """
    :param merged_df: combined values of old and new files
    :param save: flag to save graph
    :param show: flag to show graph
    :return: None
    """
    confusion_matrix = pd.crosstab(merged_df['ValueOld'], merged_df['ValueNew'])
    sn.heatmap(confusion_matrix, annot=True)
    if save:
        plt.savefig('graph_confusion.png')
    if show:
        plt.show()


def show_info(matched_samples, perc_matched_old, perc_matched_new, matched_labels_dict):
    """
    Prints number of matched samples, percentage of matched samples in old and new Files
    and number of matches for each label from old file
    :param matched_samples: Number of matched samples
    :param perc_matched_old: Percentage of matched samples in old file
    :param perc_matched_new: Percentage of matched samples in new file
    :param matched_labels_dict: Dictionary containing number of matches for each label from old file
    :return: None
    """
    print('Matched samples:{0}.'.format(matched_samples))
    print('Percentage of matched samples in old file:{0:.2f}.'.format(perc_matched_old))
    print('Percentage of matched samples in new file:{0:.2f}.'.format(perc_matched_new))
    print('Number of matches for each label from old file:')
    for row in matched_labels_dict:
        print(row)


def prepare_for_matching(old_dict, new_dict_raw):
    """
    Creates dictionary that has only samples that are present in both dictionaries
    and combines their values into a list of values
    :param old_dict:
    :param new_dict_raw:
    :return:merged dictionary
    """
    merged_values_dict = dict()
    for key in old_dict.keys():
        if key in new_dict_raw.keys():
            merged_values_dict[key] = [old_dict[key], new_dict_raw[key]]
    return merged_values_dict


if __name__ == '__main__':
    old_name = "OldFile"
    new_name = "NewFile"
    try:

        old_file = open(old_name, "r")
        new_file = open(new_name, "r")
    except BaseException as e:
        print(e)
        exit(1)

    old_dict = create_dict(old_file)
    new_dict = create_dict(new_file)
    # izrada kombiniranog dict koji sadrži elemente ID-[ValueOld,ValueNew] za plotanje confusion matrice
    merged_values_dict = prepare_for_matching(old_dict, new_dict)

    #izrada dataframa iz dictionarya

    merged_dataframe = pd.DataFrame(merged_values_dict.items(), columns=['ID', 'Value'])
    merged_dataframe[['ValueOld', 'ValueNew']] = pd.DataFrame(merged_dataframe.Value.to_list(),
                                                              index=merged_dataframe.index)
    merged_dataframe.drop(columns=['Value'], axis=1, inplace=True)

    N_matched_samples, matched_labels_dict = match_samples(old_dict, new_dict)
    matched_labels_dict = sorted(matched_labels_dict.items(), key=lambda x: x[1], reverse=True)
    perc_matched_old = N_matched_samples / len(old_dict)
    perc_matched_new = N_matched_samples / len(new_dict)

    # prikaz informacija
    show_info(N_matched_samples, perc_matched_old, perc_matched_new, matched_labels_dict)
    graph_distribution(old_dict, new_dict, save=True, show=True)
    graph_confusion(merged_dataframe, save=True, show=True)

    del old_dict
    del new_dict
    del merged_values_dict
    del merged_dataframe
    old_file.close()
    new_file.close()
